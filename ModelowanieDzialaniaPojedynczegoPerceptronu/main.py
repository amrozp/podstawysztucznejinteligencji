
import numpy
import matplotlib.pyplot as plt


from random import randint

#column[coordinate of point]
from pip._vendor.msgpack.fallback import xrange

m = 2

#rows[numbers of points for learning]
n = 200

# tab - collection of points to learning
tab = [[randint(-5, 5) for col in range(m)] for row in range(n)]
for x in tab:
 print(x)
# collection of function value
t = [0 for col in range(n)]




# random value for b and w
b = randint(0, 1)
w1 = randint(0, 1)
w2 = randint(0, 1)
w = numpy.matrix([w1, w2])

valueVector = [w1, w2]
i = 0
for coordinate in tab:
    y = coordinate[0]
    x = coordinate[1]
    left = y
    right= x+b
    if left < right:
        t[i] = 1
        plt.plot(x, y, 'b.')
    else:
        t[i] = 0
        plt.plot(x, y, 'r.')
    i = i+1






def teacher(w,b,t):
    e = [0 for col in range(n)]
    i=0
    for x in tab:
        i=i+1
        # step1
        x = numpy.matrix(x)
        x = numpy.transpose(x)
        # step2
        while True:
            a = w * x + b
            #step3
            if a > 0:
                x[1] = 1
            else:
                x[1] = 0

            e[i-1] = t[i-1]-x[1]

            if e[i-1] == 0:
                break
            w = w + e[i-1]*numpy.transpose(x)
            b = b + e[i-1]

    print(e)




teacher(w,b,t)


plt.show()
print(t)





