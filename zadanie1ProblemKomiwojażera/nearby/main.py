import math
import random
import string
import datetime
import matplotlib.pyplot as plt


class City:
    def __init__(self, x, y, name):
        self.x = x
        self.y = y
        self.name = name

    def __repr__(self):
        return self.name


city1 = City(10, 0, "A")
city2 = City(4, 2, "B")
city3 = City(2, 2, "C")
city4 = City(5, 1, "D")
city5 = City(5, 10, "E")

cities = [city1, city2, city3, city4, city5]
total_road = 0


def random_point(range = 100):
    return random.randint(0, range)


def city_factory(n):
    cities_array = []
    while len(cities_array) < n:
        cities_array.append(City(random_point(), random_point(), random.choice(string.ascii_letters)))

    return cities_array


def print_map(cities_map):
    fig, ax = plt.subplots()
    ax.scatter([city.x for city in cities_map], [city.y for city in cities_map])
    for city in cities_map:
        ax.annotate(city.name, (city.x, city.y))
    plt.xlabel('X')
    plt.ylabel('Y')
    plt.show()


def count_diff(start_city, end_city):
    return math.sqrt((start_city.x - end_city.x)**2 + (start_city.y - end_city.y)**2)


def find_closest_city(cities_map, current_city, visited):

    closest_city = None
    diff = 1000
    i = 0

    while i < len(cities_map):
        city = cities_map[i]
        if not(city.name in visited) and city.name != current_city.name:
            current_city_diff = count_diff(current_city, city)

            if current_city_diff < diff:
                diff = current_city_diff
                closest_city = city
        i += 1

    return closest_city


def nearby_neighbour(cities_map, start_city):
    current_city = start_city
    visited = []

    while len(visited) < len(cities_map):
        to_visit = find_closest_city(cities_map, current_city, visited + [current_city.name])
        visited.append(to_visit.name)
        current_city = to_visit

    print(f"Total road length: {round(total_road, 2)}")
    print(' -> '.join(visited))


def check_maps():

    cities_map = city_factory(5)
    for city in cities_map:
        print(city.name)

    start = datetime.datetime.now()
    nearby_neighbour(cities_map, cities_map[0])
    finish = datetime.datetime.now()

    print(finish - start)
    # print_map(cities_map)


check_maps()
